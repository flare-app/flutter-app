part of 'user_bloc.dart';

@immutable
abstract class UserEvent {}

class UserEvent2 extends UserEvent {}

class InitializeUserEvent extends UserEvent {}

class ListenToUser extends UserEvent {
  final String uid;
  ListenToUser(this.uid);
}

class UpdateUser extends UserEvent {
  final User user;
  UpdateUser(this.user);
}

class NoUserEvent extends UserEvent {}

class LogOut extends UserEvent {}
