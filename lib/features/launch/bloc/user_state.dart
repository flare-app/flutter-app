part of 'user_bloc.dart';

@immutable
abstract class UserState {}

class UserInitial extends UserState {}

class NoCurrentUserState extends UserState {}

class UserRetrieved extends UserState {
  final User user;
  UserRetrieved(this.user);
}

class UserUpdated extends UserState {
  final User user;
  UserUpdated(this.user);
}
