import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flare/features/launch/repository/firebase_user_repository.dart';
import 'package:flare/model/user.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

part 'user_event.dart';
part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  User user;
  FirebaseUserRepository userRepository = FirebaseUserRepository();

  UserBloc() {
    this.add(InitializeUserEvent());
  }

  @override
  UserState get initialState => UserInitial();

  //TODO: Consider spliting auth and user bloc into two, or use phone auth for currentuser on root - leaving this one only as user repo
  @override
  Stream<UserState> mapEventToState(
    UserEvent event,
  ) async* {
    if (event is InitializeUserEvent) {
      userRepository.userAuth().listen((uid) {
        this.add(uid != null ? ListenToUser(uid) : NoUserEvent());
      });
    }

    if (event is ListenToUser) {
      userRepository.userData(uid: event.uid).listen((userUpdate) {
        this.add(UpdateUser(user));
      });
    }

    if (event is UpdateUser) {
      yield this.user == null
          ? UserRetrieved(event.user)
          : UserUpdated(event.user);
      user = event.user;
    }

    if (event is NoUserEvent) {
      yield NoCurrentUserState();
    }

    if (event is LogOut) {
      await userRepository.logOut();
      yield NoCurrentUserState();
    }

    print(event);
  }
}
