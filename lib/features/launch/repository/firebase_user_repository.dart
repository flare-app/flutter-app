import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flare/model/user.dart';

class FirebaseUserRepository {
  FirebaseAuth auth = FirebaseAuth.instance;
  Firestore db = Firestore.instance;

  Stream<String> userAuth() {
    return auth.onAuthStateChanged.map((result) => result?.uid);
  }

  Stream<User> userData({String uid}) {
    return db
        .collection('users')
        .document(uid)
        .snapshots()
        .map((userDoc) => User.withMap(userDoc.documentID, userDoc.data));
  }

  Future logOut() {
    try {
      auth.signOut();
    } catch (err) {
      //Handle error
    }
  }
}
