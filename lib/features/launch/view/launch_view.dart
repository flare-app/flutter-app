import 'package:flare/constants/routes.dart';
import 'package:flare/features/launch/bloc/user_bloc.dart';
import 'package:flare/features/onboarding/view/onboarding_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LaunchView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<UserBloc, UserState>(
        listener: (context, state) {
          _navigateWith(context, state);
        },
        //UI Should match the launcher/splash views
        child: Center(
          child: Text("Launch"),
        ),
      ),
    );
  }

  _navigateWith(BuildContext context, UserState state) async {
    if (state is UserRetrieved) {
      await Navigator.of(context).pushNamed(Routes.feedRoute);
      BlocProvider.of<UserBloc>(context)..add(InitializeUserEvent());
    }

    if (state is NoCurrentUserState) {
      print("No current user");
      Navigator.of(context).popUntil(ModalRoute.withName(Routes.launchRoute));
      Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => OnboardingView(0)),
      );
    }
  }
}
