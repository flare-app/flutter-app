import 'package:flare/features/launch/bloc/user_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FeedView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<UserBloc, UserState>(
        listener: (context, state) {
          print(state);
        },
        //UI Should match the launcher/splash views
        child: RaisedButton(
            onPressed: () => BlocProvider.of<UserBloc>(context)..add(LogOut()),
            child: Center(child: Text("Feed"))),
      ),
    );
  }
}
