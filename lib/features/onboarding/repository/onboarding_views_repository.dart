import 'package:flare/constants/strings.dart';
import 'package:flare/model/onboarding_data.dart';

class OnboardingDataRepository {
  static final List<OnboardingData> data = [
    OnboardingData(
      Strings.onboardingOneTitle,
      Strings.onboardingOneDescription,
    ),
    OnboardingData(
      Strings.onboardingTwoTitle,
      Strings.onboardingTwoDescription,
    ),
    OnboardingData(
      Strings.onboardingThreeTitle,
      Strings.onboardingThreeDescription,
    ),
  ];
}
