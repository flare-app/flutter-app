import 'package:flare/common_widgets/expanded_container.dart';
import 'package:flare/constants/routes.dart';
import 'package:flare/constants/sizes.dart';
import 'package:flare/features/onboarding/repository/onboarding_views_repository.dart';
import 'package:flare/model/onboarding_data.dart';
import 'package:flutter/material.dart';

class OnboardingView extends StatelessWidget {
  final int index;
  OnboardingView(this.index);

  @override
  Widget build(BuildContext context) {
    OnboardingData data = OnboardingDataRepository.data[index];
    bool isFinalView = index == OnboardingDataRepository.data.length - 1;

    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(Sizes.standardPadding),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  ExpandedContainer(),
                  FlatButton(
                    onPressed: () {
                      Navigator.of(context)
                          .pushNamed(Routes.createAccountRoute);
                    },
                    child: Text("Skip"),
                  ),
                ],
              ),

              SizedBox(height: Sizes.standardInteritemSpace),

              //TODO: Replace for image
              Container(
                width: Sizes.fullWidth(context, padding: Sizes.standardPadding),
                height:
                    Sizes.fullWidth(context, padding: Sizes.standardPadding),
                color: Colors.lime,
              ),

              SizedBox(height: Sizes.largeInteritemSpace),

              Text(data.title),

              SizedBox(height: Sizes.smallInteritemSpace),

              Text(
                data.description,
                textAlign: TextAlign.center,
              ),

              ExpandedContainer(),

              isFinalView
                  ? Column(
                      children: <Widget>[
                        FlatButton(
                          onPressed: () {
                            Navigator.of(context)
                                .pushNamed(Routes.createAccountRoute);
                          },
                          child: Text("Get started"),
                        ),
                      ],
                    )
                  : Row(
                      children: <Widget>[
                        ExpandedContainer(),
                        FlatButton(
                          onPressed: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => OnboardingView(index + 1),
                              ),
                            );
                          },
                          child: Text("Next"),
                        ),
                      ],
                    )
            ],
          ),
        ),
      ),
    );
  }
}
