import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flare/features/login_signup/bloc/phone_authentication_bloc.dart';
import 'package:flare/model/user.dart';

class FirebaseAuthRepository {
  var auth = FirebaseAuth.instance;

  Future<PhoneAuthenticationState> verifyPhoneNumber(String number) async {
    Completer<PhoneAuthenticationState> completer = Completer();

    print("Verify requested");

    try {
      await auth.verifyPhoneNumber(
        phoneNumber: number,
        timeout: Duration(seconds: 30),
        verificationCompleted: (authCredential) => _signInWithAuthCredential(
          authCredential,
          completer,
        ),
        verificationFailed: _onVerificationFailed,
        codeSent: (verificationId, [ints]) => _onCodeSent(
          verificationId,
          completer,
        ),
        codeAutoRetrievalTimeout: (str) => print("Code Retrieval Failed"),
      );
    } catch (err) {
      //TODO: Complete with error
      print('Error: ${err}');
    }

    return completer.future;
  }

  Future<PhoneAuthenticationState> signInWithOTP(
    String otp,
    String verificationId,
  ) async {
    print("OTP event triggered with $verificationId");
    Completer<PhoneAuthenticationState> completer = Completer();

    try {
      AuthCredential authCredential = PhoneAuthProvider.getCredential(
        verificationId: verificationId,
        smsCode: otp,
      );

      _signInWithAuthCredential(authCredential, completer);
    } catch (err) {
      //TODO: Handle Error
    }

    return completer.future;
  }

  _signInWithAuthCredential(
    AuthCredential authCredential,
    Completer completer,
  ) async {
    print("Signing in");
    try {
      AuthResult value = await auth.signInWithCredential(authCredential);

      if (value.user != null) {
        User user = User(value.user.uid);
        completer.complete(UserSignedInState(user));
      } else {
        //TODO: Throw error
      }
    } catch (err) {
      //TODO: Handle error
    }
  }

  _onCodeSent(
    String verificationId,
    Completer completer,
  ) {
    print("Code sent with $verificationId");
    completer.complete(OTPSentState(verificationId));
  }

  _onVerificationFailed(AuthException error) {
    //TODO: Throw Error
    print('Error: ${error.message}');
  }
}
