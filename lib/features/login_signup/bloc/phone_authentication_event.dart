part of 'phone_authentication_bloc.dart';

@immutable
abstract class PhoneAuthenticationEvent {}

class SignUpRequestedEvent extends PhoneAuthenticationEvent {
  final String number;
  SignUpRequestedEvent(this.number);
}

class OTPSubmittedEvent extends PhoneAuthenticationEvent {
  final String otp;
  final String verificationId;
  OTPSubmittedEvent(this.otp, this.verificationId);
}
