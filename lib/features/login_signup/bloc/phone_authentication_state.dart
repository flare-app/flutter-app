part of 'phone_authentication_bloc.dart';

@immutable
abstract class PhoneAuthenticationState {}

class PhoneAuthenticationInitial extends PhoneAuthenticationState {}

class OTPSentState extends PhoneAuthenticationState {
  final String verificationId;
  OTPSentState(this.verificationId);
}

class UserSignedInState extends PhoneAuthenticationState {
  final User user;
  UserSignedInState(this.user);
}

class SignInErrorState extends PhoneAuthenticationState {
  final String errorMessage;
  SignInErrorState(this.errorMessage);
}
