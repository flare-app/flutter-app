import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flare/features/login_signup/repository/firebase_auth_repository.dart';
import 'package:flare/model/user.dart';
import 'package:meta/meta.dart';

part 'phone_authentication_event.dart';
part 'phone_authentication_state.dart';

class PhoneAuthenticationBloc
    extends Bloc<PhoneAuthenticationEvent, PhoneAuthenticationState> {
  final FirebaseAuthRepository authRepository = FirebaseAuthRepository();

  String verificationId;

  @override
  PhoneAuthenticationState get initialState => PhoneAuthenticationInitial();

  @override
  Stream<PhoneAuthenticationState> mapEventToState(
    PhoneAuthenticationEvent event,
  ) async* {
    print(event);
    if (event is SignUpRequestedEvent) {
      PhoneAuthenticationState state =
          await authRepository.verifyPhoneNumber(event.number);
      print(state);
      yield state;
    }

    if (event is OTPSubmittedEvent) {
      print("Event submitted");
      PhoneAuthenticationState state =
          await authRepository.signInWithOTP(event.otp, event.verificationId);
      print(state);
      yield state;
    }
  }
}
