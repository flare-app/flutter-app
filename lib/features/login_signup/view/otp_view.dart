import 'package:flare/features/login_signup/bloc/phone_authentication_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class OTPView extends StatelessWidget {
  final String verificationId;
  final PhoneAuthenticationBloc phoneAuthBloc;

  OTPView(this.verificationId, this.phoneAuthBloc);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => phoneAuthBloc,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: TextField(
              onSubmitted: (otp) {
                print("Submitting otp");
                phoneAuthBloc.add(
                  OTPSubmittedEvent(otp, this.verificationId),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
