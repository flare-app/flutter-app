import 'package:firebase_auth/firebase_auth.dart';
import 'package:flare/constants/strings.dart';
import 'package:flare/features/login_signup/bloc/phone_authentication_bloc.dart';
import 'package:flare/features/login_signup/view/otp_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CreateAccountView extends StatelessWidget {
  final PhoneAuthenticationBloc phoneAuthBloc = PhoneAuthenticationBloc();

  final TextEditingController phoneNumberController =
      TextEditingController(text: '+13055551234');

  @override
  Widget build(BuildContext context) {

    _onLoginPressed() async {
      phoneAuthBloc.add(SignUpRequestedEvent(phoneNumberController.text));
    }

    return Scaffold(
      body: BlocProvider(
        create: (context) => phoneAuthBloc,
        child: BlocListener<PhoneAuthenticationBloc, PhoneAuthenticationState>(
          listener: (context, state) {
            if (state is OTPSentState) {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) =>
                      OTPView(state.verificationId, phoneAuthBloc),
                ),
              );
            }
          },

          child: SafeArea(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(56.0, 56.0, 56.0, 16.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    Strings.createAccountTitle,
                    style: Theme.of(context).textTheme.title,
                  ),
                  SizedBox(height: 16.0),
                  Text(
                    Strings.createAccountDescription,
                    style: Theme.of(context).textTheme.body1,
                  ),
                  Expanded(child: 
                  Form(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        TextFormField(
                          decoration: InputDecoration(
                            labelText: Strings.createAccountPhonePrompt,
                            labelStyle: Theme.of(context).textTheme.overline,
                          ),
                          controller: phoneNumberController,
                          keyboardType: TextInputType.phone,
                        ),
                        Expanded(
                          child: Container(),
                        ),
                        RaisedButton(
                          padding: EdgeInsets.all(16.0),
                          onPressed: () async {
                            _onLoginPressed();
                          },
                          child: Text(Strings.next),
                        ),
                      ],
                    ),
                  )
                  ),
                ]
              ),
            ),
          ),
        ),
      ),
    );
  }
}
