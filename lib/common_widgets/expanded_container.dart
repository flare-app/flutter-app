import 'package:flutter/material.dart';

class ExpandedContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(child: Container());
  }
}
