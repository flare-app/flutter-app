import 'package:flare/constants/routes.dart';
import 'package:flare/features/feed/view/feed_view.dart';
import 'package:flare/features/launch/bloc/user_bloc.dart';
import 'package:flare/features/login_signup/view/create_account_view.dart';
import 'package:flare/features/launch/view/launch_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(
    BlocProvider(
      create: (context) => UserBloc(),
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          backgroundColor: Colors.transparent,
          primarySwatch: Colors.blue,
          textTheme: TextTheme(
            title: TextStyle(fontSize: 20),
            overline: TextStyle(fontSize: 16),
            body1: TextStyle(fontSize: 16, color: Color(0xFF505050)),
          )),
      routes: {
        Routes.launchRoute: (context) => LaunchView(),
        Routes.createAccountRoute: (context) => CreateAccountView(),
        Routes.feedRoute: (context) => FeedView(),
      },
      initialRoute: Routes.launchRoute,
    );
  }
}
