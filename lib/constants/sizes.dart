import 'package:flutter/material.dart';

class Sizes {
  //Margins
  static const double standardPadding = 20;

  //Spacing
  static const double smallInteritemSpace = 8;
  static const double standardInteritemSpace = 20;
  static const double largeInteritemSpace = 40;

  //Screen Dimensions
  static fullWidth(BuildContext context, {double padding = 0}) {
    return MediaQuery.of(context).size.width - padding;
  }

  static fullHeight(BuildContext context, {double padding = 0}) {
    return MediaQuery.of(context).size.height - padding;
  }
}
