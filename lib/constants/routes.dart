class Routes {
  static const String launchRoute = 'launch';
  static const String onboardingRoute = 'onboarding';
  static const String createAccountRoute = 'create_account';
  static const String feedRoute = 'feed';
}
