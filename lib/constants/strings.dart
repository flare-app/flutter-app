class Strings {
  //Account Creation
  static const String createAccountTitle = "Create an account";
  static const String createAccountDescription =
      "We will use your mobile number to create the account, login and to help you stay in touch.\n\nWe'll keep all the information safe, and won't share it.";
  static const String createAccountPhonePrompt = "What's your mobile number?";

  //Onboarding
  static const String onboardingOneTitle = "Be a Community HERO";
  static const String onboardingOneDescription =
      "Offer help to people near you, in the neighbourhood.";
  static const String onboardingTwoTitle = "Or ask people for a favour";
  static const String onboardingTwoDescription =
      "Volunteers can help you with groceries, walking your dog or picking up meds.";
  static const String onboardingThreeTitle = "Join Flare community";
  static const String onboardingThreeDescription =
      "Help us support each other and let's have a chat!";

  static const String alreadyHaveAccount = "Already have an account?";
  static const String login = "Login";

  //Buttons
  static const String next = "Next";
  static const String submit = "Submit";
  static const String getStarted = "Get started";
}
