class OnboardingData {
  final String title;
  final String description;
  final String imagePath;

  OnboardingData(this.title, this.description, {this.imagePath});
}
