import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:test/test.dart' as Test;
import 'package:flare/features/login_signup/bloc/phone_authentication_bloc.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  group('PhoneAuthBloc', () {
    blocTest(
      'phone authentication requested - otpsentstate is returned',
      build: () => PhoneAuthenticationBloc(),
      act: (bloc) => bloc.add(SignUpRequestedEvent('+13055551234')),
      wait: Duration(seconds: 10),
      expect: [
        Test.isA<PhoneAuthenticationInitial>(),
        Test.isA<OTPSentState>()
      ],
    );
  });
}
